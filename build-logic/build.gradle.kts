plugins { `kotlin-dsl` }

dependencies { implementation(libs.spotless) }

afterEvaluate {
    tasks.withType<JavaCompile>().configureEach {
        sourceCompatibility = JavaVersion.VERSION_17.toString()
        targetCompatibility = JavaVersion.VERSION_17.toString()
    }
}

gradlePlugin {
    plugins {
        register("spotless") {
            version = "1.0.0"
            id = "foundation.e.blissweather.spotless"
            implementationClass = "foundation.e.blissweather.SpotlessPlugin"
        }
        register("githooks") {
            version = "1.0.0"
            id = "foundation.e.blissweather.githooks"
            implementationClass = "foundation.e.blissweather.GitHooksPlugin"
        }
    }
}
