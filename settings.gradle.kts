@file:Suppress("UnstableApiUsage")

pluginManagement {
    repositories {
        includeBuild("build-logic")
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven("https://gitlab.e.foundation/api/v4/groups/9/-/packages/maven")
        maven("https://gitlab.e.foundation/api/v4/groups/1391/-/packages/maven")
        maven("https://jitpack.io")
    }
}

enableFeaturePreview("STABLE_CONFIGURATION_CACHE")

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

include(":android", ":data", ":preferences")

rootProject.name = "BlissWeather"
