/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.models

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

enum class Units {
    IMPERIAL,
    METRIC
}

@Keep
@Serializable
@Parcelize
data class Coordinate(
    val lat: Double,
    val lon: Double,
) : Parcelable
