/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.models

import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep @Serializable data class WeatherDayResponse(val list: List<WeatherDay>)

@Keep
@Serializable
data class WeatherDay(
    val temp: DayTemps,
    val weather: List<Weather>,
)

@Keep
@Serializable
data class DayTemps(
    val day: Double,
    val min: Double,
    val max: Double,
)
