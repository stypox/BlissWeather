/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.models

import androidx.annotation.Keep
import com.squareup.moshi.Json
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class Weather(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String,
)

@Keep
@Serializable
data class Main(
    val temp: Double? = null,
    @Json(name = "feels_like") @SerialName("feels_like") val feelsLike: Double? = null,
    @Json(name = "temp_min") @SerialName("temp_min") val tempMin: Double? = null,
    @Json(name = "temp_max") @SerialName("temp_max") val tempMax: Double? = null,
    val pressure: Double? = null,
    val humidity: Double? = null,
)

@Keep
@Serializable
data class Wind(
    val speed: Double? = null,
    val deg: Int? = null,
)

@Keep
@Serializable
data class Clouds(
    val all: Int? = null,
)

@Keep
@Serializable
data class Rain(
    val oneHour: Double? = null,
    val threeHours: Double? = null,
)

@Keep
@Serializable
data class Snow(
    val oneHour: Double? = null,
    val threeHours: Double? = null,
)

@Keep
@Serializable
data class Sys(
    val type: Int? = null,
    val id: Int? = null,
    val country: String? = null,
    val sunrise: Long? = null,
    val sunset: Long? = null,
)

@Keep
@Serializable
data class CombinedWeatherResponse(
    val current: WeatherData? = null,
    val forecasts: WeatherDayResponse? = null,
)
