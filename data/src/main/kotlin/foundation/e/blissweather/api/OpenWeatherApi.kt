/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.api

import foundation.e.blissweather.models.Units
import foundation.e.blissweather.models.WeatherCity
import foundation.e.blissweather.models.WeatherData
import foundation.e.blissweather.models.WeatherDayResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {
    interface Base {
        @GET("weather")
        suspend fun getWeatherByCoords(
            @Query("lat") latitude: Double,
            @Query("lon") longitude: Double,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
        ): Response<WeatherData>

        @GET("weather")
        suspend fun getWeatherByLocationName(
            @Query("q") name: String,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
        ): Response<WeatherData>

        @GET("forecast/daily")
        suspend fun getForecastByCoords(
            @Query("lat") latitude: Double,
            @Query("lon") longitude: Double,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
            @Query("cnt") count: Int = 6,
        ): Response<WeatherDayResponse>
    }

    interface Geo {
        @GET("direct")
        suspend fun getLocationCoordsByName(
            @Query("q") name: String,
            @Query("limit") limit: Int = 1,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
        ): Response<List<WeatherCity>>

        @GET("reverse")
        suspend fun getLocationNameByCoords(
            @Query("lat") latitude: Double,
            @Query("lon") longitude: Double,
            @Query("limit") limit: Int = 1,
            @Query("appid") apiKey: String,
            @Query("units") units: Units = Units.METRIC,
            @Query("lang") lang: String = "en",
        ): Response<List<WeatherCity>>
    }

    companion object {
        const val BASE_API_URL = "https://api.openweathermap.org/data/2.5/"
        const val BASE_GEOCODING_URL = "http://api.openweathermap.org/geo/1.0/"
    }
}
