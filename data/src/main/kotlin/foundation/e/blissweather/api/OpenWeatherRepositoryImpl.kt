/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.api

import foundation.e.blissweather.ApiResult
import foundation.e.blissweather.AppPreferences
import foundation.e.blissweather.handleApi
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.models.Units
import foundation.e.blissweather.models.WeatherCity
import foundation.e.blissweather.models.WeatherData
import foundation.e.blissweather.models.WeatherDayResponse
import javax.inject.Inject

class OpenWeatherRepositoryImpl
@Inject
constructor(
    private val baseApi: OpenWeatherApi.Base,
    private val geoApi: OpenWeatherApi.Geo,
    prefs: AppPreferences
) : OpenWeatherRepository {
    private val appPreferences = prefs

    private val apiKey: String
        get() = appPreferences.weatherApiKey!!

    private val unit: Units
        get() {
            return if (appPreferences.weatherUseMetric) {
                Units.METRIC
            } else {
                Units.IMPERIAL
            }
        }

    override suspend fun getWeatherByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherData> = handleApi {
        baseApi.getWeatherByCoords(coord.lat, coord.lon, apiKey, unit)
    }

    override suspend fun getDaysForecastByCoords(
        coord: Coordinate,
    ): ApiResult<WeatherDayResponse> = handleApi {
        baseApi.getForecastByCoords(coord.lat, coord.lon, apiKey, unit)
    }

    override suspend fun getWeatherByLocationName(
        name: String,
    ): ApiResult<WeatherData> = handleApi { baseApi.getWeatherByLocationName(name, apiKey, unit) }

    override suspend fun getLocationCoordsByName(
        name: String,
        limit: Int,
    ): ApiResult<List<WeatherCity>> = handleApi {
        geoApi.getLocationCoordsByName(name, limit, apiKey, unit)
    }

    override suspend fun getLocationNameByCoords(
        coord: Coordinate,
        limit: Int,
    ): ApiResult<List<WeatherCity>> = handleApi {
        geoApi.getLocationNameByCoords(coord.lat, coord.lon, limit, apiKey, unit)
    }
}
