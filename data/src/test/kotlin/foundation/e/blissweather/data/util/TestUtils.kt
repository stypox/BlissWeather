/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.data.util

import com.google.common.truth.Truth.assertThat
import java.io.File
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
object TestUtils {
    fun getJson(path: String): String {
        val uri = requireNotNull(javaClass.classLoader).getResource(path)
        val file = File(uri.path)
        return String(file.readBytes())
    }

    inline fun <reified T> assertIs(value: Any?): T {
        contract { returns() implies (value is T) }
        assertThat(value).isInstanceOf(T::class.java)
        return value as T
    }
}
