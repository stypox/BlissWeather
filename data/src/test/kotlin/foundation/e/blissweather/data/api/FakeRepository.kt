/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.data.api

import foundation.e.blissweather.ApiResult
import foundation.e.blissweather.ApiSuccess
import foundation.e.blissweather.api.OpenWeatherRepository
import foundation.e.blissweather.data.util.TestUtils.getJson
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.models.WeatherCity
import foundation.e.blissweather.models.WeatherData
import foundation.e.blissweather.models.WeatherDayResponse
import kotlinx.serialization.json.Json

class FakeOpenWeatherRepository : OpenWeatherRepository {
    private val json = Json { ignoreUnknownKeys = true }

    override suspend fun getWeatherByCoords(coord: Coordinate): ApiResult<WeatherData> {
        val data: WeatherData = json.decodeFromString(getJson("weather_by_coords.json"))
        return ApiSuccess(data)
    }

    override suspend fun getDaysForecastByCoords(coord: Coordinate): ApiResult<WeatherDayResponse> {
        val data: WeatherDayResponse = json.decodeFromString(getJson("weather_days_forecast.json"))
        return ApiSuccess(data)
    }

    override suspend fun getWeatherByLocationName(name: String): ApiResult<WeatherData> {
        val data: WeatherData = json.decodeFromString(getJson("weather_by_location.json"))
        return ApiSuccess(data)
    }

    override suspend fun getLocationCoordsByName(
        name: String,
        limit: Int
    ): ApiResult<List<WeatherCity>> {
        val data: List<WeatherCity> = json.decodeFromString(getJson("location_coords_by_name.json"))
        return ApiSuccess(data)
    }

    override suspend fun getLocationNameByCoords(
        coord: Coordinate,
        limit: Int
    ): ApiResult<List<WeatherCity>> {
        val data: List<WeatherCity> = json.decodeFromString(getJson("location_name_by_coords.json"))
        return ApiSuccess(data)
    }
}
