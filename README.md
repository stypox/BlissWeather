# BlissWeather

Standalone Weather application for Android using [OpenWeatherMap API](https://openweathermap.org/api).

## How to install?

1. Download the latest release from [pipeline](https://gitlab.e.foundation/e/os/BlissWeather/-/pipelines/latest?ref=develop).

2. Install the release apk on your device running [/e/OS](https://e.foundation/e-os/).

3. Go to _Settings > Apps > BlissWeather > Permissions_ and grant the **Allow access all the time** location permission. Also, make sure **Use precise location** is enabled.

4. Now go to the -1 screen and add the new BlissWeather widget to the list.

5. Enjoy! It should update the weather data and display it on the widget in a while.

## License

```
Copyright © MURENA SAS 2023.

All rights reserved. This program and the accompanying materials
are made available under the terms of the GNU Public License v3.0
which accompanies this distribution, and is available at
http://www.gnu.org/licenses/gpl.html
```
