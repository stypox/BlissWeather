@file:Suppress("UnstableApiUsage", "DSL_SCOPE_VIOLATION")

plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
}

android {
    namespace = "foundation.e.blissweather.preferences"
    compileSdk = 33

    defaultConfig { minSdk = 25 }

    buildFeatures { buildConfig = true }

    buildTypes {
        configureEach {
            buildConfigField("String", "OWM_API_KEY", "\"${System.getenv("OWM_KEY")}\"")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
}

dependencies {
    implementation(libs.androidx.core.ktx)
    implementation(libs.google.dagger.hilt)
}
