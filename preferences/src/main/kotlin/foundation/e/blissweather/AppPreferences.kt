/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather

import android.content.SharedPreferences
import androidx.core.content.edit
import foundation.e.blissweather.preferences.BuildConfig
import javax.inject.Inject

class AppPreferences @Inject constructor(private val sharedPrefs: SharedPreferences) {

    val all: Map<String, *>
        get() = sharedPrefs.all

    var weatherApiKey: String?
        get() = sharedPrefs.getString(Constants.WEATHER_API_KEY, BuildConfig.OWM_API_KEY)!!
        set(value) {
            sharedPrefs.edit { putString(Constants.WEATHER_API_KEY, value?.trim()) }
        }

    var weatherRefreshInterval: Long
        get() = sharedPrefs.getString(Constants.WEATHER_REFRESH_INTERVAL, "15")!!.toLong()
        set(value) {
            sharedPrefs.edit {
                putString(Constants.WEATHER_REFRESH_INTERVAL, value.toString().trim())
            }
        }

    var weatherUseCustomLocation: Boolean
        get() = sharedPrefs.getBoolean(Constants.WEATHER_USE_CUSTOM_LOCATION, false)
        set(value) {
            sharedPrefs.edit { putBoolean(Constants.WEATHER_USE_CUSTOM_LOCATION, value) }
        }

    var weatherCustomLocationCity: String
        get() = sharedPrefs.getString(Constants.WEATHER_CUSTOM_LOCATION_CITY, "")!!
        set(value) {
            sharedPrefs.edit { putString(Constants.WEATHER_CUSTOM_LOCATION_CITY, value.trim()) }
        }

    var weatherCustomLocationLat: Double
        get() = sharedPrefs.getString(Constants.WEATHER_CUSTOM_LOCATION_LAT, "0")!!.toDouble()
        set(value) {
            sharedPrefs.edit {
                putString(Constants.WEATHER_CUSTOM_LOCATION_LAT, value.toString().trim())
            }
        }

    var weatherCustomLocationLon: Double
        get() = sharedPrefs.getString(Constants.WEATHER_CUSTOM_LOCATION_LON, "0")!!.toDouble()
        set(value) {
            sharedPrefs.edit {
                putString(Constants.WEATHER_CUSTOM_LOCATION_LON, value.toString().trim())
            }
        }

    var weatherUseMetric: Boolean
        get() = sharedPrefs.getBoolean(Constants.WEATHER_USE_METRIC, true)
        set(value) {
            sharedPrefs.edit { putBoolean(Constants.WEATHER_USE_METRIC, value) }
        }
}
