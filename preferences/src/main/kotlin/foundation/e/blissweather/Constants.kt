/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather

object Constants {
    const val WEATHER_SOURCE = "weather_source"
    const val WEATHER_API_KEY = "weather_api_key"
    const val WEATHER_REFRESH_INTERVAL = "weather_refresh_interval"
    const val WEATHER_USE_CUSTOM_LOCATION = "weather_use_custom_location"
    const val WEATHER_CUSTOM_LOCATION_CITY = "weather_custom_location_city"
    const val WEATHER_CUSTOM_LOCATION_LAT = "weather_custom_location_lat"
    const val WEATHER_CUSTOM_LOCATION_LON = "weather_custom_location_lon"
    const val WEATHER_ICON_SET = "weather_icons"
    const val WEATHER_USE_METRIC = "weather_use_metric"
}
