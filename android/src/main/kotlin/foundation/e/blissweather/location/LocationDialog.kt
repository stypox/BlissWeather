/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.location

import android.app.AlertDialog
import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.models.WeatherCity

class LocationDialog : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val alertDialogBuilder = AlertDialog.Builder(requireContext())
        val cities =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                arguments?.getParcelableArrayList(LOCATIONS_KEY, WeatherCity::class.java)
            } else {
                arguments?.getParcelableArrayList(LOCATIONS_KEY)
            }
        val cityNames = cities?.map { "${it.name} (${it.country})" }?.toTypedArray()

        Log.d(TAG, "onCreateDialog: $cities")

        alertDialogBuilder.apply {
            setTitle("Select location")
            setPositiveButton("Cancel") { dialog, _ -> dialog.dismiss() }
            setSingleChoiceItems(cityNames, -1) { dialog, selection ->
                val selectedCity = cities?.get(selection)
                if (selectedCity != null) {
                    setFragmentResult(
                        SELECTION_KEY,
                        bundleOf(SELECTION_VALUE to Coordinate(selectedCity.lat, selectedCity.lon))
                    )
                }

                dialog.dismiss()
            }
        }

        return alertDialogBuilder.create()
    }

    companion object {
        fun newInstance(): LocationDialog = LocationDialog()

        const val TAG = "CustomLocationPreference"
        const val LOCATIONS_KEY = "locations"
        const val SELECTION_KEY = "selection"
        const val SELECTION_VALUE = "selection_value"
    }
}
