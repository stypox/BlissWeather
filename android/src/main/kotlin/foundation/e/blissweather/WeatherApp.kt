/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather

import android.app.Application
import android.util.Log
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import foundation.e.lib.telemetry.Telemetry.init
import javax.inject.Inject
import timber.log.Timber

@HiltAndroidApp
class WeatherApp : Application(), Configuration.Provider {
    @Inject lateinit var workerFactory: HiltWorkerFactory

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setMinimumLoggingLevel(Log.VERBOSE)
            .setWorkerFactory(workerFactory)
            .build()

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        if (!BuildConfig.DEBUG) {
            try {
                init(BuildConfig.SENTRY_DSN, this, true)
            } catch (e: Exception) {
                Timber.tag(TAG).e("Failed to initialize Sentry SDK: $e")
            }
        }

        Thread.currentThread().setUncaughtExceptionHandler { thread, throwable ->
            Timber.tag(TAG).e(throwable)

            val defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()
            defaultUncaughtExceptionHandler?.uncaughtException(thread, throwable)
        }
    }

    companion object {
        private const val TAG = "WeatherApp"
    }
}
