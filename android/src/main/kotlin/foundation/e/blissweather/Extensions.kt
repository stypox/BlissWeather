/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import kotlin.math.roundToInt

private object Extensions {
    fun degreeToDirection(context: Context, degree: Int): String {
        val directions = context.resources.getStringArray(R.array.wind_direction)
        val index = (degree / 45.0).roundToInt()
        return (directions[index % 8])
    }
}

fun Int.toDirection(context: Context) = Extensions.degreeToDirection(context, this)

fun Double.toKmph(): Int = (this * 3.6).roundToInt()

fun Context.locationPermissionEnabled(): Boolean {
    return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
        PackageManager.PERMISSION_GRANTED ||
        ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED
}
