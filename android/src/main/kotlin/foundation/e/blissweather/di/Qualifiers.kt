/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.di

import javax.inject.Qualifier

@Qualifier @Retention(AnnotationRetention.BINARY) annotation class BaseRetrofit

@Qualifier @Retention(AnnotationRetention.BINARY) annotation class GeoRetrofit

@Qualifier @Retention(AnnotationRetention.BINARY) annotation class ApplicationScope
