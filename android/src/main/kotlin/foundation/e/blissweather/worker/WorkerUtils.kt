/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.worker

import android.util.Log
import foundation.e.blissweather.ApiError
import foundation.e.blissweather.ApiException
import foundation.e.blissweather.ApiSuccess
import foundation.e.blissweather.api.OpenWeatherRepository
import foundation.e.blissweather.models.CombinedWeatherResponse
import foundation.e.blissweather.models.Coordinate

object WorkerUtils {
    private const val TAG = "WorkerUtils"

    suspend fun getCombinedWeatherData(
        weatherRepository: OpenWeatherRepository,
        coords: Coordinate,
    ): CombinedWeatherResponse {
        val current =
            when (val result = weatherRepository.getWeatherByCoords(coords)) {
                is ApiSuccess -> result.data
                is ApiError -> {
                    Log.e(TAG, "[${result.code}] Current Weather Error: ${result.message}")
                    null
                }
                is ApiException -> {
                    Log.e(TAG, "Current Weather Error:", result.e)
                    null
                }
            }

        val forecasts =
            when (val result = weatherRepository.getDaysForecastByCoords(coords)) {
                is ApiSuccess -> result.data
                is ApiError -> {
                    Log.e(TAG, "[${result.code}] Forecasts Error: ${result.message}")
                    null
                }
                is ApiException -> {
                    Log.e(TAG, "Forecasts Error:", result.e)
                    null
                }
            }

        return CombinedWeatherResponse(current, forecasts)
    }
}
