/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.worker

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.util.Log
import android.widget.RemoteViews
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import foundation.e.blissweather.AppPreferences
import foundation.e.blissweather.R
import foundation.e.blissweather.api.OpenWeatherRepository
import foundation.e.blissweather.location.LocationProvider
import foundation.e.blissweather.models.CombinedWeatherResponse
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.widget.WeatherAppWidgetProvider
import foundation.e.blissweather.widget.updateWeatherView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@HiltWorker
class WeatherUpdaterWorker
@AssistedInject
constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val locationProvider: LocationProvider,
    private val appPreferences: AppPreferences,
    private val weatherRepository: OpenWeatherRepository,
) : CoroutineWorker(appContext, workerParams) {
    private val appWidgetManager by lazy { AppWidgetManager.getInstance(applicationContext) }

    override suspend fun doWork(): Result {
        val weather = loadWeather()
        val widgetIds =
            appWidgetManager.getAppWidgetIds(
                ComponentName(applicationContext, WeatherAppWidgetProvider::class.java)
            )

        Log.d("WeatherUpdaterWorker", "Updating widget ${widgetIds.toList()}")
        Log.d("WeatherUpdaterWorker", "Weather is $weather")

        return if (weather?.current == null || weather.forecasts == null) {
            Result.retry()
        } else {
            val views =
                RemoteViews(applicationContext.packageName, R.layout.layout_weather_info).apply {
                    updateWeatherView(applicationContext, weather, appPreferences)
                }

            appWidgetManager.updateAppWidget(widgetIds, views)
            Result.success()
        }
    }

    private suspend fun loadWeather(): CombinedWeatherResponse? =
        withContext(Dispatchers.IO) {
            Log.d("WeatherUpdaterWorker", "Requesting weather data")
            var weatherData: CombinedWeatherResponse? = null
            val useCustomLoc =
                appPreferences.weatherUseCustomLocation &&
                    (appPreferences.weatherCustomLocationLat != .0 ||
                        appPreferences.weatherCustomLocationLon != .0)

            if (useCustomLoc) {
                val coords =
                    Coordinate(
                        appPreferences.weatherCustomLocationLat,
                        appPreferences.weatherCustomLocationLon
                    )
                Log.d("WeatherUpdaterWorker", "Using custom location $coords")
                weatherData = WorkerUtils.getCombinedWeatherData(weatherRepository, coords)
            } else {
                locationProvider.getCurrentLocation()?.let {
                    Log.d("WeatherUpdaterWorker", "Using last known location $it")
                    weatherData =
                        WorkerUtils.getCombinedWeatherData(
                            weatherRepository,
                            Coordinate(it.latitude, it.longitude)
                        )
                }
            }

            return@withContext weatherData
        }
}
