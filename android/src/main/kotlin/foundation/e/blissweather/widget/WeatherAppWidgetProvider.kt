/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.widget

import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.BackoffPolicy
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import dagger.hilt.android.AndroidEntryPoint
import foundation.e.blissweather.AppPreferences
import foundation.e.blissweather.worker.WeatherUpdaterWorker
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class WeatherAppWidgetProvider : AppWidgetProvider() {
    @Inject lateinit var appPrefs: AppPreferences

    override fun onEnabled(context: Context) {
        val workMgr = WorkManager.getInstance(context.applicationContext)
        val refreshInterval = appPrefs.weatherRefreshInterval
        val constraints =
            Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()

        if (refreshInterval != 0L) {
            workMgr.enqueueUniquePeriodicWork(
                WEATHER_UPDATE,
                ExistingPeriodicWorkPolicy.UPDATE,
                PeriodicWorkRequestBuilder<WeatherUpdaterWorker>(refreshInterval, TimeUnit.MINUTES)
                    .setConstraints(constraints)
                    .build()
            )
        } else {
            workMgr
                .beginWith(
                    OneTimeWorkRequestBuilder<WeatherUpdaterWorker>()
                        .addTag(WEATHER_UPDATE)
                        .setBackoffCriteria(BackoffPolicy.LINEAR, 2, TimeUnit.MINUTES)
                        .setConstraints(constraints)
                        .build()
                )
                .enqueue()
        }
    }

    override fun onReceive(context: Context, intent: Intent?) {
        Log.d(TAG, "onReceive: $intent}")
        super.onReceive(context, intent)
        when (intent?.action) {
            ACTION_WEATHER_REFRESH -> {
                onDisabled(context)
                onEnabled(context)
            }
        }
    }

    override fun onDisabled(context: Context) {
        WorkManager.getInstance(context).cancelAllWorkByTag(WEATHER_UPDATE)
    }

    companion object {
        const val TAG = "WeatherAppWidget"
        const val WEATHER_UPDATE = "weather_update_work"

        const val ACTION_WEATHER_REFRESH = "foundation.e.blissweather.widget.ACTION_WEATHER_REFRESH"
    }
}
