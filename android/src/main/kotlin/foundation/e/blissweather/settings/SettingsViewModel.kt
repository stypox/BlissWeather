/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import foundation.e.blissweather.ApiError
import foundation.e.blissweather.ApiException
import foundation.e.blissweather.ApiSuccess
import foundation.e.blissweather.AppPreferences
import foundation.e.blissweather.api.OpenWeatherRepository
import foundation.e.blissweather.models.Coordinate
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

@HiltViewModel
class SettingsViewModel
@Inject
constructor(
    private val repository: OpenWeatherRepository,
    private val appPreferences: AppPreferences
) : ViewModel() {
    private val _state = MutableSharedFlow<SettingsState>()
    val state = _state.asSharedFlow()

    private val _status = MutableSharedFlow<String>()
    val status = _status.asSharedFlow()

    fun onCustomLocationSet() {
        viewModelScope.launch { onCustomLocationSetImpl() }
    }

    fun onApiKeyChanged() {
        viewModelScope.launch { onApiKeyChangedImpl() }
    }

    fun setCoordinate(coordinate: Coordinate) {
        appPreferences.weatherCustomLocationLat = coordinate.lat
        appPreferences.weatherCustomLocationLon = coordinate.lon
    }

    private suspend fun onCustomLocationSetImpl() {
        if (!appPreferences.weatherUseCustomLocation) return

        val city = appPreferences.weatherCustomLocationCity
        if (city.isEmpty()) {
            _status.emit("Please enter a city name")
        } else {
            _state.emit(SettingsState.Loading)
            when (val result = repository.getLocationCoordsByName(city, 5)) {
                is ApiSuccess -> {
                    if (result.data.isNotEmpty()) {
                        _state.emit(SettingsState.Success(result.data))
                    }
                }
                is ApiError -> {
                    _state.emit(SettingsState.Error)
                    _status.emit("Invalid city name")
                }
                is ApiException -> {
                    _state.emit(SettingsState.Error)
                    _status.emit(result.e.message ?: "Unknown error")
                }
            }
        }
    }

    private suspend fun onApiKeyChangedImpl() {
        when (val result = repository.getWeatherByLocationName("London")) {
            is ApiError -> {
                if (result.code == 401) {
                    appPreferences.weatherApiKey = null
                    _status.emit("Invalid API Key")
                }
            }
            is ApiException -> {
                appPreferences.weatherApiKey = null
                _status.emit(result.e.message ?: "Unknown error")
            }
            else -> {}
        }
    }
}
