/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.settings

import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceFragmentCompat
import dagger.hilt.android.AndroidEntryPoint
import foundation.e.blissweather.Constants
import foundation.e.blissweather.R
import foundation.e.blissweather.databinding.ActivitySettingsBinding
import foundation.e.blissweather.location.LocationDialog
import foundation.e.blissweather.models.Coordinate
import foundation.e.blissweather.widget.WeatherAppWidgetProvider
import javax.inject.Inject
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class SettingsActivity : FragmentActivity(), OnSharedPreferenceChangeListener {
    private val viewModel by viewModels<SettingsViewModel>()

    private lateinit var binding: ActivitySettingsBinding
    @Inject lateinit var sharedPrefs: SharedPreferences

    private val refreshIntent by lazy {
        Intent(this, WeatherAppWidgetProvider::class.java).apply {
            action = WeatherAppWidgetProvider.ACTION_WEATHER_REFRESH
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setActionBar(binding.actionBar)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.content_frame, SettingsFragment())
            .commit()

        supportFragmentManager.setFragmentResultListener(LocationDialog.SELECTION_KEY, this) {
            _,
            bundle ->
            val coordinate = bundle.getParcelable<Coordinate>(LocationDialog.SELECTION_VALUE)
            if (coordinate != null) {
                viewModel.setCoordinate(coordinate)
                sendBroadcast(refreshIntent)
            }
        }

        observeFlows()
        sharedPrefs.registerOnSharedPreferenceChangeListener(this)
    }

    private fun observeFlows() {
        viewModel.status
            .flowWithLifecycle(lifecycle)
            .onEach { Toast.makeText(this, it, Toast.LENGTH_SHORT).show() }
            .launchIn(lifecycleScope)

        viewModel.state
            .flowWithLifecycle(lifecycle)
            .onEach { state ->
                when (state) {
                    is SettingsState.Success -> {
                        LocationDialog.newInstance()
                            .apply {
                                arguments = bundleOf(LocationDialog.LOCATIONS_KEY to state.data)
                            }
                            .show(supportFragmentManager, LocationDialog.TAG)
                    }
                    else -> {}
                }
            }
            .launchIn(lifecycleScope)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        when (key) {
            Constants.WEATHER_API_KEY -> viewModel.onApiKeyChanged()
            Constants.WEATHER_CUSTOM_LOCATION_CITY -> viewModel.onCustomLocationSet()
            else -> sendBroadcast(refreshIntent)
        }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.preferences_weather, rootKey)
        }
    }
}
