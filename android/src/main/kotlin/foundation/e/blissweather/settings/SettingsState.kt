/*
 * Copyright © MURENA SAS 2023.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */
package foundation.e.blissweather.settings

import androidx.annotation.Keep
import foundation.e.blissweather.models.WeatherCity

sealed class SettingsState {
    object Loading : SettingsState()

    @Keep data class Success(val data: List<WeatherCity>) : SettingsState()

    object Error : SettingsState()
}
